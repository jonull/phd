import os
import pandas as pd
import matplotlib.pyplot as plt
import glob
import re
from my_utils import color_dict, tech_names, print_red, print_cyan, print_green, print_magenta, print_blue, print_yellow
from order_cap import wind, PV, baseload, peak, CCS, CHP, midload, hydro, PtH, order_cap, order_cap2
from datetime import datetime

# Path to the pickle files and figures
pickle_folder = 'PickleJar/'
figures_folder = 'figures/capacity/'

# Hardcoded scenario selection
scenario_selection = ['scenario_1', 'scenario_2']  # Modify with your actual scenario names
exclude_scenarios = ['scenario_3', 'scenario_4']   # Modify with the scenarios you want to exclude

# Groups of technologies 
tech_groups = {
    'Hydro': hydro,
    'PtH': PtH,
    'Wind': wind,
    'PV': PV,
    'Peak': peak,
    'Other thermals': CCS + CHP + ["W"]
}
tech_groups2 = {
    "Battery": ["bat_cap", "bat"],
    "Hydrogen": ["H2store", "electrolyser","FC"],
    "VRE": PV + wind,
    "Thermals": CCS + CHP + midload + ["W", "U", "Other thermals"] + peak,
    #"Peak": peak,
}
techs_to_exclude = PtH + ["Electrolyser", "electrolyser", "H", "b", "H_CHP", "B_CHP"]
storage_techs = ["bat", "H2store"]
storage_techs = storage_techs + [tech_names[t] for t in storage_techs if t in tech_names]

def shorten_year(scenario):
    # define a function to be used in re.sub
    def replacer(match):
        return "'" + match.group()[-2:]

    # use re.sub to replace all occurrences of 4-digit years
    return re.sub(r'(19|20)\d{2}', replacer, scenario).removeprefix("singleyear_")

def select_pickle(use_defaults):
    pickle_files = glob.glob(os.path.join(pickle_folder, "data_results_*.pickle"))
    if not pickle_files:
        print_red("No data_results_*.pickle file found in PickleJar folder.")
        return None
    
    pickle_files.sort(key=os.path.getmtime, reverse=True)
    print_blue(f"Found {len(pickle_files)} data_results_*.pickle files.")
    print_blue(f"Most recent file: {pickle_files[0]}")

    if use_defaults or len(pickle_files) == 1:
        # Either use defaults or no appropriate pickle files were found, so just use the most recent file
        #most_recent_file = max(pickle_files, key=lambda x: os.path.getctime(pickle_folder + x))
        return pickle_files[0]

    print_yellow("Select the pickle file to load:")
    print_yellow("1. Most recent file")
    print_yellow("2. Largest file")
    print_yellow("3. Hardcoded filename")
    print_yellow("4. Pick among the 10 most recent files")
    print_yellow("5. Enter the filename manually")

    user_input = input("Please enter the option number: ")
    if user_input == '1':
        # Most recent file
        return pickle_files[0]  # The list is already sorted by modification time
    elif user_input == '2':
        # Largest file
        pickle_files.sort(key=os.path.getsize, reverse=True)
        return pickle_files[0]
    elif user_input == '3':
        # Hardcoded filename
        hardcoded_filename = pickle_folder + 'data_results_20230529_162027.pickle'  # Replace with the filename you want
        if hardcoded_filename in pickle_files:
            return hardcoded_filename
        else:
            print_red("The hardcoded file was not found in the directory. Falling back to the most recent file.")
            return pickle_files[0] # The list is already sorted by modification time
    elif user_input == '4':
        # Pick among the 10 most recent files
        print_yellow("Pick among the 10 most recent files:")
        for i, f in enumerate(pickle_files[:10]):
            print_yellow(f"{i+1}. {f}")
        user_input = input("Please enter the option number: ")
        try:
            return pickle_files[int(user_input)-1]
        except:
            print_red("Invalid input. Falling back to the most recent file.")
            return pickle_files[0]
    elif user_input == '5':
        # Enter the filename manually
        user_input = input("Please enter the filename: ")
        if user_input in pickle_files or "PickleJar\\" + user_input in pickle_files or "PickleJar\\"+user_input+".pickle" in pickle_files:
            if ".pickle" not in user_input:
                user_input = user_input + ".pickle"
            if "PickleJar\\" not in user_input:
                user_input = "PickleJar\\" + user_input
            return user_input
        else:
            print_red("The file was not found in the directory. Falling back to the most recent file.")
            return pickle_files[0]


def load_data(pickle_file, use_defaults):
    # Load the pickle file
    data = pd.read_pickle(pickle_file)
    
    # Handle scenario selection
    all_scenarios = list(data.keys())
    print_cyan(f"All scenarios: {all_scenarios}")
    
    selected_scenarios = []
    if use_defaults:
        # Use all scenarios, but if there's a scenarioname with "1h", skip the one with "3h" if there is one
        selected_scenarios = [i for i in all_scenarios if "singleyear" not in i]
        print_magenta(f"Included sets: {selected_scenarios}")
        # add the singleyear scenarios corresponding to 2012, 2016-2017, 1996-1997, 2002-2003, 2003-2004, 2009-2010
        years_to_add = [i for i in all_scenarios if "singleyear" in i and ("singleyear_1h_2012" in i or "singleyear_2016to2017_" in i or "singleyear_1996to1997_" in i or "singleyear_2002to2003_" in i or "singleyear_2003to2004_" in i or "singleyear_2009to2010_" in i)]
        selected_scenarios = selected_scenarios + years_to_add

        for s in selected_scenarios:
            if "1h" in s:
                selected_scenarios = [s for s in selected_scenarios if s != s.replace("1h", "3h")]
                break
            # then do the same to skip the "tight" scenarios
            if "tight" not in s:
                selected_scenarios = [s for s in selected_scenarios if s != s + "tight"]
                break
    else:
        # Let the user exclude some scenarios
        excluded = input("Please enter the scenarios you want to exclude, separated by commas (or H for the hardcoded list): ").split(',')
        if excluded == ['H'] or excluded == ['h']:
            # Use the hardcoded list
            selected_scenarios = [#'singleyear_1989to1990_1h', 'singleyear_1995to1996_1h',
                                  'singleyear_1996to1997_1h', #'singleyear_1997to1998_1h',
                                  'singleyear_2002to2003_1h', 'singleyear_2003to2004_1h', #'singleyear_2004to2005_1h',
                                  'singleyear_2009to2010_1h', #'singleyear_2010to2011_1h', 'singleyear_2018to2019_1h', 'singleyear_2014to2015_1h',
                                  'singleyear_1h_2012', 'singleyear_2016to2017_1h',
                                  'set1_1opt', 'set1_2opt', 'set1_3opt', 'set1_4opt'] # Replace with the hardcoded list
        else:
            print_red(f"Excluding scenarios: {excluded}")
            # the input is a string but if there is an , in the input it will be split into a list
            if ',' not in excluded:
                excluded = [e.strip().replace("'", "").replace('"', '') for e in excluded]
            else:
                parts = excluded.split(',')
                print_red(f"parts: {parts}")
                excluded = [p.strip().replace("'", "").replace('"', '') for p in parts]
            selected_scenarios = [s for s in all_scenarios if s not in excluded]

    # Handle alternative scenarios
    for s in selected_scenarios:
        if s not in all_scenarios:
            # Check for alternative scenarios in all possible combinations
            alt_scenarios = [s.replace("1h", "3h"), s + "tight", s.replace("1h", "3h") + "tight",
                             s.replace("3h", "1h"), s.replace("3h", "1h") + "tight"]
            alt_scenario_found = False
            for alt_s in alt_scenarios:
                if alt_s in all_scenarios:
                    selected_scenarios.append(alt_s)
                    alt_scenario_found = True
                    print_yellow(f"Alternative scenario found for the missing {s}: {alt_s}")
                    break
            if not alt_scenario_found:
                print_red(f"No alternative scenarios found for {s}. Skipping that one...")
               
    # Extract 'tot_cap' data for the selected scenarios and replace NaNs with 0
    selected_data = {scenario: data[scenario]['tot_cap'].fillna(0) for scenario in selected_scenarios}

    # Remove "ref_cap" from scenario names 
    selected_data = {s.replace("ref_cap_", ""): selected_data[s] for s in selected_data.keys()}
    #selected_data = {s.replace("singleyear_", ""): selected_data[s] for s in selected_data.keys()}
    selected_data = {s.replace("to", "-"): selected_data[s] for s in selected_data.keys()}
    selected_data = {s.replace("_1h", ""): selected_data[s] for s in selected_data.keys()}
    selected_data = {s.replace("_tight", ""): selected_data[s] for s in selected_data.keys()}

    # has_altscenarios should be True if there are any scenarios with "v2" in the name
    has_altscenarios = any("v2" in s or "_5_" in s for s in selected_data.keys())
    if has_altscenarios and not use_defaults:
        print_yellow("There are alternative scenarios in the data. What should be done with these?")
        print_yellow("1. Keep all scenarios (default)")
        print_yellow("2. Remove alternative scenarios")
        user_input = input("Please enter your choice ( or 2): \n")
        if user_input == "1":
            print_yellow("Keeping all scenarios")
            pass
        elif user_input == "2":
            print_yellow("Removing all alternative scenarios")
            selected_data = {s: selected_data[s] for s in selected_data.keys() if "v2" not in s and "_5_" not in s}
        else:
            print_yellow("Invalid input. Keeping all scenarios")
    if has_altscenarios and use_defaults:
        print_yellow("There are alternative scenarios in the data.")
    # reorder the keys in alphabetical order
    selected_data = {k: selected_data[k] for k in sorted(selected_data.keys())}
    selected_scenarios_to_print = "\n".join(selected_data.keys())
    print_blue(f"Selected scenarios: \n{selected_scenarios_to_print}")
    return selected_data


def group_technologies(data):
    # Create a dictionary of Series to hold the grouped data
    grouped_data = {s:pd.Series(dtype=float) for s in data.keys()}

    for scenario in data.keys():
        # Sum over regions and replace technologies that belong to a group in tech_groups with the group
        data_region_sum = data[scenario].groupby(level=0).sum()

        # Iterate over each item in the Series
        for idx, value in data_region_sum.items():
            tech = idx
            if tech in techs_to_exclude:
                continue
            grouped = False
            
            # Check each group to see if the technology is in that group
            for group, tech_list in tech_groups.items():
                if tech in tech_list:
                    # Add the value to the group
                    if group not in grouped_data[scenario]:
                        grouped_data[scenario][group] = value
                    else:
                        grouped_data[scenario][group] += value
                    grouped = True
                    break

            # If the technology was not grouped, add it to the grouped data as is
            if not grouped:
                grouped_data[scenario][idx] = value

    return grouped_data


def prettify_scenario_name(name):
    #print_yellow(f"Prettifying scenario name: {name}")
    if "set1" in name:
        #print_yellow("Set 1 scenario detected")
        # turn set1_4opt into Set 1 (4 opt.)
        nr = name.split("_")[1].replace("opt", "")
        alt = " alt."*('alt' in name)
        return f"Set 1 ({nr} opt.)" + alt
    if "allopt" in name:
        # turn allopt2_final into All opt. (2 yr), and allopt2_final_a into All opt. (2 yr) a
        nr = name.split("_")[0].replace("allopt", "")
        if len(name.split("_")) == 3:
            abc = name.split("_")[2]
        else:
            abc = ""
        return f"Set 2{abc} ({nr} yr.)"
    if "iter2_3" in name:
        return "Set (1 opt.)"
    elif "iter3_16start" in name:
        return "Set (2 opt.)"
    if "singleyear" in name:
        # turn 'singleyear_1989to1990_1h' into "'89-'90" using regex
        return shorten_year(name)
    # remove 'base' and 'extreme' and split into a list
    parts = name.replace('base', '').replace('extreme', ' ').split()
    if "v2" in name or "_5_" in name:
        return f'Alt. set ({parts[0]} opt.)'
    elif "even" in name:
        return f'6 yr, eq. weights'
    # join the parts with appropriate labels
    return f'Set ({parts[0]} opt.)'


def create_figure(grouped_data, pickle_timestamp, use_defaults):
    # Create a directory for the figures if it doesn't already exist
    if not os.path.exists('figures/capacity'):
        os.makedirs('figures/capacity')

    # Create a figure and axis
    fig, ax1 = plt.subplots(figsize=(8,5))

    # Combine all scenarios into a single DataFrame
    combined_data = pd.DataFrame({scenario: data for scenario, data in grouped_data.items()})
    print_cyan(combined_data)
    # Order the bars according to order_cap
    combined_data = combined_data.reindex(order_cap2).dropna(how='all')

    # If the technology exists as a key in tech_names, replace the index with the value
    combined_data = combined_data.rename(index=tech_names, errors='ignore')
    print_magenta(combined_data)
    # Move "electrolyser" index to the top of the df and make it negative
    #combined_data = combined_data.reindex(['electrolyser'] + [idx for idx in combined_data.index if idx != 'electrolyser'])
    #combined_data.loc["electrolyser"] *= -1
    
    # Split the data into two groups: normal tech and storage tech
    normal_tech = combined_data.drop(index=storage_techs, errors='ignore')
    storage_tech = combined_data.loc[combined_data.index.intersection(storage_techs)].dropna(how='all')
    # Create second axis that shares the same x-axis
    ax2 = ax1.twinx()

    # Width of the bars
    width = 0.39

    # Plot normal tech and storage tech side by side
    bars1 = normal_tech.T.plot(kind='bar', stacked=True, ax=ax1, width=width, color=[color_dict.get(tech, 'gray') for tech in normal_tech.index], position=1.05, rot=11)
    bars2 = storage_tech.T.plot(kind='bar', stacked=True, ax=ax2, width=width, color=[color_dict.get(tech, 'gray') for tech in storage_tech.index], position=-0.05)

    # After plotting, iterate over the bars and add labels
    def conditional_label(bar, cutoff):
        # Get the height of the bar
        height = bar.get_height()
        # If height is greater than or equal to cutoff, return label
        if abs(height) >= 100:
            return f'{height:.0f}'
        # Otherwise, return an empty string
        elif abs(height) >= cutoff:
            return f'{height:.0f}'
        else:
            return ''

    # Apply this function to each bar
    for container in bars1.containers:
        labels1 = [conditional_label(bar, 40) for bar in container]
        ax1.bar_label(container, labels=labels1, label_type='center', fontsize=8)

    for container in bars2.containers:
        labels2 = [conditional_label(bar, 40) for bar in container]
        ax2.bar_label(container, labels=labels2, label_type='center', fontsize=7)
    
    # Adjust the xlim
    ax1.set_xlim(-0.5, len(combined_data.columns) - 0.5)

    # Get the legend labels and handles from both axes
    handles1, labels1 = ax1.get_legend_handles_labels()
    handles2, labels2 = ax2.get_legend_handles_labels()

    # Remove the old legends
    ax1.get_legend().remove()
    ax2.get_legend().remove()

    # Get legend location from user
    if not use_defaults:
        print("Choose a position for the legend: ")
        print("1) Top")
        print("2) Bottom")
        print("3) Right")
        legend_position = input("Enter a number (default is 1): ")

        if legend_position == '2':
            legend_loc = 'upper center'
            legend_bbox_to_anchor = (0.5, -0.25)
            legend_ncol = 4
        elif legend_position == '3':
            legend_loc = 'center left'
            legend_bbox_to_anchor = (1.1, 0.5)
            legend_ncol = 1
        else:
            legend_loc = 'lower center'
            legend_bbox_to_anchor = (0.5, 1.12)
            legend_ncol = 4
    else:
        legend_loc = 'lower center'
        legend_bbox_to_anchor = (0.5, 1.12)
        legend_ncol = 4

    # Create a combined legend above the figure title
    ax1.legend(handles1 + handles2, labels1 + labels2, loc=legend_loc, ncol=legend_ncol, bbox_to_anchor=legend_bbox_to_anchor)

    # Change the x-labels to be right-aligned
    x_ticks = ax1.get_xticklabels()
    new_labels = []#[f"{f'{scenario}, '*(len(scenario.replace(year,''))>3)}" + f"{year}" for scenario, year in zip(all_scenarios, all_years)]
    for scenario in x_ticks:
        scenario = scenario.get_text()
        if len(scenario) > 10 or "_" in scenario:
            # change labels from "base#extreme#" (where # is a number) to "#b #e"
            temp_label = f"{scenario.replace('_tight','').replace('_1h','')}"
            temp_label = prettify_scenario_name(temp_label)
            new_labels.append(temp_label)
        else:
            new_labels.append(scenario)
    ax1.set_xticklabels(new_labels, rotation=20, ha='right', fontsize=10)

    # Add title and labels
    ax1.set_title('Technology capacity in each scenario')
    ax1.set_xlabel('Scenario')
    ax1.set_ylabel('Installed power capacity [GW]')
    ax2.set_ylabel('Installed storage capacity [GWh]')
    plt.tight_layout(pad=0.5)
    
    # Save the figure as PNG and SVG (or EPS)
    fig_name_base = f"figures/capacity/{pickle_timestamp}"
    fig_num = 1
    while os.path.exists(f"{fig_name_base}_{fig_num}.png"):
        fig_num += 1
    fig.savefig(f"{fig_name_base}_{fig_num}.png", dpi=300)
    fig.savefig(f"{fig_name_base}_{fig_num}.svg")  # or .eps for EPS format

    # Close the figure to free memory
    plt.close(fig)

    print_green(f"Figure saved as '{fig_name_base}_{fig_num}.png'.")

def create_figure_separated_techs(grouped_data, pickle_timestamp, use_defaults):
    # Create a directory for the figures if it doesn't already exist
    if not os.path.exists('figures/capacity'):
        os.makedirs('figures/capacity')

    # Create a figure and axes
    fig, axs = plt.subplots((len(tech_groups2)-1)//2+1, 2, figsize=(7,3*len(tech_groups2)//2)) 
    axes = axs.flatten()

    # Combine all scenarios into a single DataFrame
    combined_data = pd.DataFrame({scenario: data for scenario, data in grouped_data.items()})
    print_magenta(combined_data)
    
    # Loop over each group and create a subplot
    for ax, (group_name, tech_list) in zip(axes, tech_groups2.items()):
        # Filter data for the current technology group
        group_data = combined_data.loc[combined_data.index.intersection(tech_list)].dropna(how='all')
        
        # Plot the data for this group
        group_data.T.plot(kind='bar', stacked=True, ax=ax, color=[color_dict.get(tech, 'gray') for tech in group_data.index], width=0.8)
        
        # Set the title for this subplot
        ax.set_title(group_name)

        # After plotting, iterate over the bars and add labels
        def conditional_label(bar, cutoff):
            # Get the height of the bar
            height = bar.get_height()
            # If height is greater than or equal to cutoff, return label
            if abs(height) >= 100:
                return f'{height:.0f}'
            # Otherwise, return an empty string
            elif abs(height) >= cutoff:
                return f'{height:.0f}'
            else:
                return ''
        
        # get the max y-value for this subplot
        max_y = ax.get_ylim()[1]
        for bars in ax.containers:
            # Apply this function to each bar
            for bar in bars:
                #if in the Battery or Hydrogen group, make the label text white
                if group_name in ['Battery', 'Hydrogen']:
                    textcolor = 'white'
                else:
                    textcolor = 'black'
                label = conditional_label(bar, 10)
                ax.text(bar.get_x() + bar.get_width() / 2, bar.get_y() + bar.get_height()-max_y*0.04, label, ha='center', va='center', fontsize=6, color=textcolor)

    # Add overall title, labels, legend etc. to your liking
    # Change the x-labels to be right-aligned
    for i_a, ax in enumerate(axes):
        x_ticks = ax.get_xticklabels()
        new_labels = []#[f"{f'{scenario}, '*(len(scenario.replace(year,''))>3)}" + f"{year}" for scenario, year in zip(all_scenarios, all_years)]
        for scenario in x_ticks:
            scenario = scenario.get_text()
            if len(scenario) > 10 or "_" in scenario:
                # change labels from "base#extreme#" (where # is a number) to "#b #e"
                temp_label = f"{scenario.replace('_tight','').replace('_1h','')}"
                temp_label = prettify_scenario_name(temp_label)
                new_labels.append(temp_label)
            else:
                # change all years from 19## or 20## to '## (e.g. 1990 to '90)
                scenario = shorten_year(scenario).replace('_tight','').replace('1h_','')
                new_labels.append(scenario)

        ax.set_xticklabels(new_labels, rotation=35, ha='right', fontsize=10, rotation_mode='anchor')
        # for each label in the legend see if there is a better name in tech_names and replace it
        handles, labels = ax.get_legend_handles_labels()
        new_labels = []
        for label in labels:
            if label in tech_names:
                new_labels.append(tech_names[label])
            else:
                new_labels.append(label)
        ncols = 1+(len(new_labels)>2)
        ax.legend(handles[::-1], new_labels[::-1], loc='lower center', framealpha=0.65, ncols=2, fontsize="small") # [::-1] to reverse the order of the legend entries
        if i_a==0:
            ax.set_ylabel('Installed capacity [GW(h)]')
        elif i_a == 1:
            ax.set_ylabel('Installed storage capacity [GWh]')
        else:
            ax.set_ylabel('Installed power capacity [GW]')
    fig.tight_layout(pad=0.5)
    plt.subplots_adjust(wspace=0.29)
    # Save and close the figure as in your original function
    # Save the figure as PNG and SVG (or EPS)
    fig_name_base = f"figures/capacity/{pickle_timestamp}"
    fig_num = 1
    while os.path.exists(f"{fig_name_base}_{fig_num}.png"):
        fig_num += 1
    fig.savefig(f"{fig_name_base}_{fig_num}.png", dpi=300)
    fig.savefig(f"{fig_name_base}_{fig_num}.svg")  # or .eps for EPS format
    print_green(f"Figure saved as '{fig_name_base}_{fig_num}.png'.")

    # Close the figure to free memory
    plt.close(fig)



def main():
    print_blue(f"Script started at: {datetime.now()}")
    
    user_input = input("Press ENTER to go with default options or type anything to be prompted for choices along the way: ")
    use_defaults = user_input.strip() == ""  # This will be True if the user just pressed enter

    print_blue(f"Script started at: {datetime.now()}")
    pickle_file = select_pickle(use_defaults)
    pickle_timestamp = pickle_filename = os.path.basename(pickle_file).replace(".pickle", "").replace("data_results_", "")
    print_cyan(f"Selected pickle file: {pickle_file}")
    
    data = load_data(pickle_file, use_defaults)
    print_yellow(f"Data loaded from pickle file")
    grouped_data = group_technologies(data)
    print_green(f"Technologies grouped successfully")
    #print_yellow(f"Grouped data: \n{grouped_data}")
    create_figure_separated_techs(grouped_data, pickle_timestamp, use_defaults)
    print_magenta(f"Figures created and saved in {figures_folder}")
    
    print_red(f"Script finished at: {datetime.now()}")

if __name__ == "__main__":
    main()
